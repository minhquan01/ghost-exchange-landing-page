import { Disclosure } from "@headlessui/react";
import subtractIcon from "../images/subtract.svg";
import addIcon from "../images/add.svg";
import { motion } from "framer-motion";
import { botToTop, childVariant, parentVariant, topToBot } from "../utils/motion";

const frequentlyAskedLists = [
	{
		question: "Why should I use Ghost Xchange?",
		answer:
			"Ghost Xchange allows you to remain completely anonymous when sending crypto. The Blockchain does not lie, everyone can trace everything right back to your wallet. You deserve to uphold your privacy and go about your activities as any normal person.",
	},
	{
		question: "How does Ghost Xchange work?",
		answer:
			"Ghost Xchange allows you to remain completely anonymous when sending crypto. The Blockchain does not lie, everyone can trace everything right back to your wallet. You deserve to uphold your privacy and go about your activities as any normal person.",
	},
	{
		question: "What fees does Ghost Xchange charges?",
		answer:
			"Ghost Xchange allows you to remain completely anonymous when sending crypto. The Blockchain does not lie, everyone can trace everything right back to your wallet. You deserve to uphold your privacy and go about your activities as any normal person.",
	},
	{
		question: "Do I need a KYC to use Ghost Xchange?",
		answer:
			"Ghost Xchange allows you to remain completely anonymous when sending crypto. The Blockchain does not lie, everyone can trace everything right back to your wallet. You deserve to uphold your privacy and go about your activities as any normal person.",
	},
	{
		question: "Why is my order is taking longer than expected?",
		answer:
			"Ghost Xchange allows you to remain completely anonymous when sending crypto. The Blockchain does not lie, everyone can trace everything right back to your wallet. You deserve to uphold your privacy and go about your activities as any normal person.",
	},
	{
		question: " How do I stake my $GHOST?",
		answer:
			"Ghost Xchange allows you to remain completely anonymous when sending crypto. The Blockchain does not lie, everyone can trace everything right back to your wallet. You deserve to uphold your privacy and go about your activities as any normal person.",
	},
];

const FrequentlyAsked = () => {
	return (
		<>
			<motion.h3
				variants={topToBot}
				initial="initial"
				whileInView="onscreen"
				className="text-[18px] xl:text-[36px] font-bold xl:leading-[64px] text-white"
				id="faq"
			>
				Frequently asked questions
			</motion.h3>
			<motion.div
				variants={parentVariant}
				initial="initial"
				whileInView="onscreen"
				className="w-full xl:w-[752px] mt-3 xl:mt-12"
			>
				{frequentlyAskedLists.map((item, index) => (
					<motion.div
						variants={childVariant}
						initial="initial"
						whileInView="onscreen"
						key={index}
					>
						<Disclosure defaultOpen={index === 0}>
							{({ open }: { open: boolean }) => (
								<>
									<div aria-expanded={open} className="p-5 group ">
										<Disclosure.Button className="flex w-full text-left justify-between rounded-lg bg-transparent">
											<span className="text-[14px] xl:text-[18px] font-semibold leading-normal text-white/50 group-aria-expanded:text-white">
												{index + 1}. {item.question}
											</span>
											<img src={open ? subtractIcon : addIcon} alt="" />
										</Disclosure.Button>
										<Disclosure.Panel className="mt-[10px] text-[12px] xl:text-[16px] font-[300] leading-normal text-white/70">
											{item.answer}
										</Disclosure.Panel>
									</div>
									<div className="mx-5 h-[1px] bg-[#1C1C1E]" />
								</>
							)}
						</Disclosure>
					</motion.div>
				))}
			</motion.div>
		</>
	);
};

export default FrequentlyAsked;

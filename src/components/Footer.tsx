import ghostLogo from "../images/ghost-logo.png";
import discordCircleIcon from "../images/discord-circle.svg";
import mediumCircleIcon from "../images/medium-circle.svg";
import twitterCircleIcon from "../images/twitter-circle.svg";
import telegramCircleIcon from "../images/telegram-circle.svg";
import { motion } from "framer-motion";
import { botToTop, topToBot } from "../utils/motion";

const Footer = () => {
	return (
		<div className="relative z-10 flex flex-col items-center">
			<motion.img
				variants={topToBot}
				initial="initial"
				whileInView="onscreen"
				src={ghostLogo}
				alt="ghost logo"
				className="w-[172px] h-[75px] object-cover"
			/>
			<motion.ul
				variants={botToTop}
				initial="initial"
				whileInView="onscreen"
				className="w-full my-6 text-[14px] xl:text-[17px] font-medium leading-normal text-white/50 flex flex-wrap justify-center px-4 gap-4 xl:px-0 xl:gap-9"
			>
				<li className="hover:text-white">
					<a href="/">About</a>
				</li>
				<li className="hover:text-white">
					<a href="https://ghostexchange.netlify.app/swap">Swap</a>
				</li>
				<li className="hover:text-white">
					<a href="https://ghostexchange.netlify.app/stake">Stake</a>
				</li>
				<li className="hover:text-white">
					<a href="https://ghostexchange.netlify.app/how-it-works">How it works</a>
				</li>
				<li className="hover:text-white">
					<a href="https://ghostexchange.netlify.app/ghost-rewards">$GHOST</a>
				</li>
			</motion.ul>
			<motion.div
				variants={botToTop}
				initial="initial"
				whileInView="onscreen"
				className="flex gap-4 mb-12"
			>
				<a href="/discord" className="transition-all ease-in-out hover:scale-105">
					<img src={discordCircleIcon} alt="" />
				</a>
				<a href="/medium" className="transition-all ease-in-out hover:scale-105">
					<img src={mediumCircleIcon} alt="" />
				</a>
				<a href="/twitter" className="transition-all ease-in-out hover:scale-105">
					<img src={twitterCircleIcon} alt="" />
				</a>
				<a href="/telegram" className="transition-all ease-in-out hover:scale-105">
					<img src={telegramCircleIcon} alt="" />
				</a>
			</motion.div>
			<div className="bg-[#ABABAB]/10 backdrop-blur-[20px] w-full py-5 text-center">
				<p className="text-[14px] xl:text-[16px] leading-normal text-white/50">
					© 2023 Ghost Swap, All rights reserved.
				</p>
			</div>
		</div>
	);
};

export default Footer;
